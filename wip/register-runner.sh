#!/bin/bash
set -ux
echo "this doesn't work, don't expect it to"
echo "go to http://$(curl icanhazip.com)/admin/runners"
docker exec -it $(docker ps -a | grep gitlab-runner | awk '{print $1}') gitlab-runner register
