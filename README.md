## air gap (noun)
- a network security measure to ensure that a secure computer network is physically isolated from unsecured networks, including the public Internet
- the unobstructed vertical space between a water outlet and the flood level of a fixture (not applicable)

### Using this repo to test GitLab Security products offline

Requirements:

VM running Ubuntu 18.04 with 4CPU threads and 8+GB RAM.

```bash
# clone airgap setup script repo
git clone https://gitlab.com/greg/airgap.git && cd airgap
# set scripts to be executable
chmod +x *.sh
# run setup script to install and start GitLab, GitLab Runner, and Docker Container Registry
sudo ./setup.sh
# set root user password in GitLab UI
navigate to http://<IP_ADDRESS> in your browser and set GitLab root user password
# docker pull/tag/push all the analyzers to local docker registry - this takes a while
sudo ./analyzerator.sh
# docker pull/tag/push sast to local docker registry
sudo ./sast-dast.sh 
# install some tools for testing/simulating airgap
sudo ./test-tools.sh
```

We're almost there!

#### Register & connect shared docker runner to your GitLab - https://docs.gitlab.com/runner/register/#docker

Navigate to `http://<IP_ADDRESS>/admin/runners` in your browser to get runner token

`docker run --rm -t -i -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register`

Set `docker` executor when prompted

Navigate to http://<IP_ADDRESS>/admin/runners and click edit on your newly registered runner

Uncheck box to allow runner to turn it into a shared runner

#### Simulate Air Gap

##### Method 1: Block outgoing traffic at the network layer using `iptables` or `ufw` 

Block outgoing/incoming traffic to registry.gitlab.com and hub.docker.com at a bare minimum

##### Method 2: Sniff network traffic to see when and what external requests are made on your network

I like [`ngrep`](https://linux.die.net/man/8/ngrep) to listen to for and sniff out specific types of traffic with little effort.

For example:

`sudo ngrep -l -q 'gitlab.com|registry.gitlab.com|hub.docker.com'`

Will return packet-capture level details about any outgoing/incoming requests to any of the endpoints in the single-quotes.

Alternatively, you can also take a `tcpdump` to save a packet capture for later analysis using wireshark, tshark etc
