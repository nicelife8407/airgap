#!/bin/bash
set -ux
analyzers=(bandit brakeman eslint flawfinder gosec kubesec klar gemnasium gemnasium-maven gemnasium-gradle-plugin gemnasium-python retire.js nodejs-scan phpcs-security-audit pmd-apex secrets security-code-scan sobelow spotbugs tslint)
gitlab=registry.gitlab.com/gitlab-org/security-products/analyzers/
registry=localhost:5000
for i in "${analyzers[@]}"
do
  echo pulling $gitlab$i:2
  docker pull $gitlab$i:2
  docker tag $(sudo docker images | grep $i | awk '{print $3}') $registry/analyzers/$i:2;
  docker push $registry/analyzers/$i;
done
